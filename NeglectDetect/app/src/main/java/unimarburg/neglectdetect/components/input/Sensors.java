package unimarburg.neglectdetect.components.input;

import android.content.Context;
import android.hardware.Sensor;
import android.hardware.SensorManager;
import android.widget.Toast;

import unimarburg.neglectdetect.components.general.Values;

/**
 * Created by Johannes on 29.11.2016.
 */

public class Sensors {
    private static Sensor Gyro, Acceleration, Motion, Step;
    private static SensorManager SManager;

    //Initialisiert die verwendeten Sensoren
    public static void initialize(Context context)
    {
        SManager = (SensorManager) context.getSystemService(Context.SENSOR_SERVICE);
        if(Acceleration == null)
        {
            if(SManager.getDefaultSensor(Sensor.TYPE_LINEAR_ACCELERATION) != null)
            {
                Acceleration = SManager.getDefaultSensor(Sensor.TYPE_LINEAR_ACCELERATION);


            }
            else
            {
                Toast.makeText(context, "Accelerometer not found!", Toast.LENGTH_SHORT).show();
            }
        }

        if(Gyro == null)
        {
            if(SManager.getDefaultSensor(Sensor.TYPE_GYROSCOPE) != null)
            {
                Gyro = SManager.getDefaultSensor(Sensor.TYPE_GYROSCOPE);


            }
            else
            {
                Toast.makeText(context, "Accelerometer not found!", Toast.LENGTH_SHORT).show();
            }
        }

        if(Motion == null)
        {
            if (SManager.getDefaultSensor(Sensor.TYPE_SIGNIFICANT_MOTION) != null)
            {
                Motion = SManager.getDefaultSensor(Sensor.TYPE_SIGNIFICANT_MOTION);
            }
            else
            {
                Toast.makeText(context, "Significant Motion Sensor not found!", Toast.LENGTH_SHORT).show();
            }
        }

        if(Step == null)
        {
            if (SManager.getDefaultSensor(Sensor.TYPE_STEP_DETECTOR) != null)
            {
                Step = SManager.getDefaultSensor(Sensor.TYPE_STEP_DETECTOR);
            }
            else
            {
                Toast.makeText(context, "Step Sensor not found!", Toast.LENGTH_SHORT).show();
            }
        }

    }

    public static Sensor getGyro() {
        return Gyro;
    }

    public static void setGyro(Sensor gyro) {
        Gyro = gyro;
    }

    public static Sensor getAcceleration() {
        return Acceleration;
    }

    public static void setAcceleration(Sensor acceleration) {
        Acceleration = acceleration;
    }

    public static SensorManager getSManager() {
        return SManager;
    }

    public static void setSManager(SensorManager SManager) {
        Sensors.SManager = SManager;
    }

    public static Sensor getMotion() {
        if (Values.isStep()) return Step;
        else return Motion;
    }

    public static void setMotion(Sensor motion) {
        Motion = motion;
    }

    public static Sensor getStep() {
        return Sensors.Step;
    }

    public static void setStep(Sensor step) {
        Sensors.Step = step;
    }
}
