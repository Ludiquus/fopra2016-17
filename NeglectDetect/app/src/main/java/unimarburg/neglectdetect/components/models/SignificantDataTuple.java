package unimarburg.neglectdetect.components.models;

import java.io.IOException;
import java.io.OutputStreamWriter;

import unimarburg.neglectdetect.components.general.Values;

/**
 * Created by Johannes on 20.01.2017.
 */

public class SignificantDataTuple extends VectorTuple {

    private byte alert; // Flash is first Bit, Sound is second Bit and Vibration is third Bit.
    // => example: 001(1) = Flash but neither Sound nor Vibration
    //             100(4) = Vibration but neither Sound nor Flash

    private boolean triggered; //true if an alert wat this point in time

    public SignificantDataTuple(VectorTuple tuple, byte b, boolean triggered)
    {
        super(tuple);
        alert = b;
        this.triggered = triggered;
    }

    public void write(OutputStreamWriter osw) throws IOException
    {
        //Writes all significant Data into a File
        osw.write(getTimestamp());
        osw.write("\t");

        osw.write(Byte.toString(alert));
        osw.write("\t");

        double calibratedAccX = getAverageAcc()- Values.getCalibratedBaseAccX();
        boolean sign = Math.signum(calibratedAccX)>=0;
        calibratedAccX = Math.abs(calibratedAccX);

        osw.write(Boolean.toString(sign));
        osw.write("\t");

        osw.write(""+calibratedAccX);
        osw.write("\t");

        osw.write(Boolean.toString(triggered));
        osw.write("\t");

        osw.write(Boolean.toString(isFlagged()));
        osw.write("\n");

        osw.flush();

    }
}
