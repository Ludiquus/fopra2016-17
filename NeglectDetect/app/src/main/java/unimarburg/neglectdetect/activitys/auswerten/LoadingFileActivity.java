package unimarburg.neglectdetect.activitys.auswerten;

import android.app.Activity;
import android.content.Intent;
import android.os.Bundle;
import android.util.Log;
import android.widget.TextView;
import android.widget.Toast;

import java.io.BufferedReader;
import java.io.File;
import java.io.FileInputStream;
import java.io.IOException;
import java.io.InputStreamReader;
import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.Date;

import unimarburg.neglectdetect.R;
import unimarburg.neglectdetect.components.general.Values;

public class LoadingFileActivity extends Activity {

    TextView loadDataAmount;
    long dataAmount = 0L;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_loading_file);
        loadDataAmount = (TextView) findViewById(R.id.loadDataAmount);
        Bundle bundle = getIntent().getExtras();
        String p = bundle.getString("path");
        try{
            loadFile(p);
        }catch(IOException e)
        {
            e.printStackTrace();
            Toast.makeText(this, "Fehler beim Laden der Datei", Toast.LENGTH_LONG).show();
        }
    }
    //Liest eine Datei aus. zeigt dabei dne Fortschritt auf dem Bildschirm.
    public void loadFile(String pfad) throws IOException
    {
        File initialFile = new File(pfad);
        FileInputStream inputStream = new FileInputStream(initialFile);
        BufferedReader br = new BufferedReader(new InputStreamReader(inputStream));
        br.readLine();//überspringen der .csv Formatierungszeile
        String s = "";
        boolean first = true;
        Date firstDate = new Date();
        Date lastDate = new Date();

        int diagnosisErrors = 0;
        long diagnosisStart = 0L;
        long diagnosisEnd = 0L;
        long diagnosisGyroSum = -1L;
        long diagnosisDataAmount = 0L;
        double diagnosisAvgAccXDiffSum = 0.0;
        double lastAAX = 0.0;

        while(true)
        {
            s = br.readLine();
            if(s==null)
                break;
            dataAmount++;
            loadDataAmount.setText(""+dataAmount);
            //Index	Timestamp	AccX	AccY	AccZ	AverageAccX	Flagged
            String sub = s.substring(s.indexOf("\t")+1);
            String date = sub.substring(0,sub.indexOf("\t"));
            DateFormat formatter = new SimpleDateFormat("dd.MM.yyyy_hh.mm.ss");
            Date d = new Date();
            try{
                d = (Date) formatter.parse(date);
            }catch (Exception e)
            {
                e.printStackTrace();
            }
            if(first)
            {
                firstDate = d;
                first=false;
            }
            else
            {
                lastDate = d;
            }
            sub = sub.substring(sub.indexOf("\t")+1);//AccX
            sub = sub.substring(sub.indexOf("\t")+1);//AccY
            sub = sub.substring(sub.indexOf("\t")+1);//AccZ
            sub = sub.substring(sub.indexOf("\t")+1);//AvgAccX
            double accX = Double.parseDouble(sub.substring(0,sub.indexOf("\t")));
            Log.d("FileManager",""+accX);
            double diff = Math.abs(accX- Values.getCalibratedBaseAccX());
            diagnosisAvgAccXDiffSum += diff;
            //if (diff >= accXTolerance && Math.signum(diff)==signum) {
            //TODO wirkt beim testen nicht richtig?
            if (diff >= Values.getAccXTolerance() && accX != lastAAX)
            {
                diagnosisErrors++;
                lastAAX = accX;
            }

            diagnosisDataAmount++;

        }

        diagnosisStart = firstDate.getTime();
        diagnosisEnd = lastDate.getTime();
        //Daten werden an den Auswertungsmodus der Diagnose übergeben
        Intent intent = new Intent(LoadingFileActivity.this, DiagnosisActivity.class);
        intent.putExtra("count", diagnosisErrors);
        intent.putExtra("startTime", diagnosisStart);
        intent.putExtra("endTime", diagnosisEnd);
        intent.putExtra("gyroSum", diagnosisGyroSum);
        intent.putExtra("avgAccXDiffSum", diagnosisAvgAccXDiffSum);
        intent.putExtra("dataAmount", diagnosisDataAmount);
        startActivity(intent);
        finish();
    }
}
