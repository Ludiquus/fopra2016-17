package unimarburg.neglectdetect.activitys.menu;

import android.content.Context;
import android.content.DialogInterface;
import android.os.Bundle;
import android.support.v7.app.AlertDialog;
import android.support.v7.app.AppCompatActivity;
import android.view.View;
import android.widget.EditText;

import unimarburg.neglectdetect.R;
import unimarburg.neglectdetect.components.general.Values;

public class DebugActivity extends AppCompatActivity {

    EditText tupleSizeEdit, sensorDelayEdit, avgAccXCountEdit, dataTimeOutEdit, alertScoreEdit, gyroToleranceEdit, accXToleranceEdit;
    Context context = this;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_debug);
        tupleSizeEdit = (EditText) findViewById(R.id.tupleSizeEdit);
        tupleSizeEdit.setText(""+Values.getTupleSize());

        sensorDelayEdit = (EditText) findViewById(R.id.sensorDelayEdit);
        sensorDelayEdit.setText(""+Values.getSensorDelay());

        avgAccXCountEdit = (EditText) findViewById(R.id.avgAccXCountEdit);
        avgAccXCountEdit.setText(""+Values.getAvgAccXcount());

        dataTimeOutEdit = (EditText) findViewById(R.id.dataTimeOutEdit);
        dataTimeOutEdit.setText(""+Values.getDataTimeOut());

        alertScoreEdit = (EditText) findViewById(R.id.alertScoreEdit);
        alertScoreEdit.setText(""+Values.getAlertScore());

        gyroToleranceEdit = (EditText) findViewById(R.id.gyroToleranceEdit);
        gyroToleranceEdit.setText(""+Values.getGyroTolerance());

        accXToleranceEdit = (EditText) findViewById(R.id.accXToleranceEdit);
        accXToleranceEdit.setText(""+Values.getAccXTolerance());

    }

    public void saveAndDismiss(View view)
    {
        Values.setTupleSize(Integer.parseInt(tupleSizeEdit.getText().toString()));
        Values.setSensorDelay(Integer.parseInt(sensorDelayEdit.getText().toString()));
        Values.setAvgAccXcount(Integer.parseInt(avgAccXCountEdit.getText().toString()));
        Values.setDataTimeOut(Integer.parseInt(dataTimeOutEdit.getText().toString()));
        Values.setAlertScore(Integer.parseInt(alertScoreEdit.getText().toString()));

        Values.setGyroTolerance(Double.parseDouble(gyroToleranceEdit.getText().toString()));
        Values.setAccXTolerance(Double.parseDouble(accXToleranceEdit.getText().toString()));

        Values.saveDebug(this);

        finish();

    }

    public void restoreDefault(View view) {
        Values.loadDefaultDebug(context);

        finish();
    }

    @Override
    public void onBackPressed() {
        AlertDialog.Builder builder = new AlertDialog.Builder(this);
        builder.setMessage("Ohne Speichern schließen?");
        builder.setPositiveButton("Ja", new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialog, int which) {
                DebugActivity.this.finish();
            }
        });
        builder.setNegativeButton("Nein", new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialog, int which) {

            }
        });
        builder.show();
    }
}
