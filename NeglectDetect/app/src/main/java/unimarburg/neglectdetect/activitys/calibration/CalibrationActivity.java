package unimarburg.neglectdetect.activitys.calibration;

import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.hardware.Sensor;
import android.hardware.SensorEvent;
import android.hardware.SensorEventListener;
import android.hardware.TriggerEventListener;
import android.os.Handler;
import android.support.v7.app.AlertDialog;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.util.Log;
import android.widget.TextView;

import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;

import unimarburg.neglectdetect.R;
import unimarburg.neglectdetect.components.input.Sensors;
import unimarburg.neglectdetect.components.general.Values;
import unimarburg.neglectdetect.activitys.menu.MenuActivity;
import unimarburg.neglectdetect.components.input.FilePath;
import unimarburg.neglectdetect.components.notification.alerts;

public class CalibrationActivity extends AppCompatActivity {

    public Double calVal = 0.0;
    private int accCount = 0;
    private int maxAccCount = 500;//Values.getCalibrationValues();
    private alerts alerts;
    private Sensor MotionSensor;
    private TriggerEventListener mTriggerEventListener;
    private SensorEventListener mTriggerStepEventListener;
    Context context = this;

    SensorEventListener AccListener;

    private TextView t;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_calibration);

        t = (TextView) findViewById(R.id.textView10);

        alerts = new alerts(this);

        AccListener = new SensorEventListener() {
            @Override
            public void onSensorChanged(SensorEvent event) {
                calibrate(event.values[0]);
            }

            @Override
            public void onAccuracyChanged(Sensor sensor, int i) {

            }
        };

        //wir arbeiten ab jetzt mit einem Delay anstatt der Sensoren
        /*if (Sensors.getMotion() != null) {
            MotionSensor = Sensors.getMotion();
            mTriggerEventListener = new TriggerEventListener() {
                @Override
                public void onTrigger(TriggerEvent event) {
                    Sensors.getSManager().cancelTriggerSensor(mTriggerEventListener, MotionSensor);
                    Sensors.getSManager().registerListener(AccListener,Sensors.getAcceleration(), Values.getSensorDelay());
                }
            };
            mTriggerStepEventListener = new SensorEventListener() {
                @Override
                public void onSensorChanged(SensorEvent sensorEvent) {
                    Sensors.getSManager().unregisterListener(this);
                    mTriggerEventListener.onTrigger(null);
                }

                @Override
                public void onAccuracyChanged(Sensor sensor, int i) {

                }
            };*/
        final Handler handler = new Handler();
        handler.postDelayed(new Runnable() {
                @Override
                public void run() {
                    Sensors.getSManager().registerListener(AccListener,Sensors.getAcceleration(), Values.getSensorDelay());
                }
        }, 5000 ); //time to delay
    }

    /**
     * calculates and saves a calibrated value that serves as the new comparison value for future measurements
     * @param accX the relevant acceleration value
     * @author Frederik
     */
    public void calibrate(float accX) {
        /*if(accCount < 0) {
            accCount += 1;
            t.setText(accCount + "");
        } else*/
        if(accCount < maxAccCount) {
            calVal += accX/maxAccCount;
            accCount += 1;
            t.setText(accCount + "");

        } else {
            if (!alerts.isVibrating()) alerts.vibrationTest();
            Sensors.getSManager().unregisterListener(AccListener);
            Log.d("Calibration",""+calVal);
            AlertDialog.Builder alertDialogBuilder = new AlertDialog.Builder(context);
            alertDialogBuilder.setMessage("Kalibrierung abgeschlossen.\nWert: " + calVal.toString());
            alertDialogBuilder.setPositiveButton("OK", new DialogInterface.OnClickListener() {
                @Override
                public void onClick(DialogInterface dialogInterface, int i) {
                    dialogInterface.dismiss();
                }
            });
            alertDialogBuilder.setOnDismissListener(new DialogInterface.OnDismissListener() {
                @Override
                public void onDismiss(DialogInterface dialogInterface) {
                    if (alerts.isVibrating()) alerts.vibrationTest();
                    Values.setCalibratedBaseAccX(calVal);
                    Values.save(CalibrationActivity.this);
                    Intent intent = new Intent(CalibrationActivity.this, MenuActivity.class);
                    finish();

                    File path = FilePath.getWorkingDirectory();

                    try {
                        File file = new File(path, "calibration.txt");
                        path.mkdirs();
                        if (!file.exists()) {
                            file.createNewFile();
                        }
                        FileOutputStream fos = new FileOutputStream(file);

                        byte[] calValInBytes = String.valueOf(calVal).getBytes();

                        fos.write(calValInBytes);
                        fos.flush();
                        fos.close();

                        System.out.println("Genitalhering");

                    } catch (FileNotFoundException e) {
                        e.printStackTrace();
                        System.out.println("PeterHeinz");
                    } catch (IOException ioe) {
                        ioe.printStackTrace();
                        System.out.println("LosWochos");
                    }

                    startActivity(intent);
                }
            });
            alertDialogBuilder.show();
        }
    }

    @Override
    public void onBackPressed() {
        super.onBackPressed();
        Values.setCalibratedBaseAccX(calVal);
        Values.save(this);
    }

    @Override
    protected void onResume() {
        super.onResume();
        /*if (Sensors.getMotion() != null) {
            if (!Values.isStep()) Sensors.getSManager().requestTriggerSensor(mTriggerEventListener, MotionSensor);
            else Sensors.getSManager().registerListener(mTriggerStepEventListener, MotionSensor, SensorManager.SENSOR_DELAY_UI);
        }*/
    }

    @Override
    protected void onPause() {
        super.onPause();
        /*if (Sensors.getMotion() != null) {
            Sensors.getSManager().cancelTriggerSensor(mTriggerEventListener, MotionSensor);
            Sensors.getSManager().unregisterListener(mTriggerStepEventListener);
        }*/
        Sensors.getSManager().unregisterListener(AccListener);
    }
}
