package unimarburg.neglectdetect.activitys;

//import android.Manifest;

import android.Manifest;
import android.app.Activity;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.os.Bundle;
import android.support.v4.app.ActivityCompat;
import android.support.v4.content.ContextCompat;
import android.support.v7.app.AlertDialog;
import android.support.v7.app.AppCompatActivity;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.view.View;
import unimarburg.neglectdetect.R;
import unimarburg.neglectdetect.components.input.Sensors;
import unimarburg.neglectdetect.components.general.Values;
import unimarburg.neglectdetect.activitys.auswerten.AuswertenActivity;
import unimarburg.neglectdetect.activitys.menu.MenuActivity;
import unimarburg.neglectdetect.activitys.messung.SensorActivity;

//import android.content.pm.PackageManager;
//import android.os.Build;
//import android.support.v4.app.ActivityCompat;
//import android.support.v4.content.ContextCompat;
//import android.widget.TextView;
//import unimarburg.neglectdetect.components.notification.alerts;

public class MainActivity extends AppCompatActivity {

    private static final int MY_PERMISSIONS_REQUEST_ACCESS_FINE_LOCATION = 0;
    private static final int MY_PERMISSIONS_REQUEST_READ_EXTERNAL_STORAGE = 0;
    private static final int MY_PERMISSIONS_REQUEST_WRITE_EXTERNAL_STORAGE = 0;
    private static final int MY_PERMISSIONS_REQUEST_CAMERA = 0;
    private Context context = this;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        checkPermissionsOnRuntime();
        setContentView(R.layout.activity_main);
        Values.loadDebug(this);
        Values.load(this);
        Sensors.initialize(this);

    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        MenuInflater inflater = getMenuInflater();
        inflater.inflate(R.menu.main_menu, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()) {
            case R.id.mainprefbutton: {
                Intent intent = new Intent(this, MenuActivity.class);
                startActivity(intent);

            }
            return true;
            default:
                return super.onOptionsItemSelected(item);

        }
    }

    /**
     * @param view View
     */
    public void openSensors(View view) {
        Intent intent = new Intent(this, SensorActivity.class);
        startActivity(intent);
    }

    /**
     * onClick methode fuer Auswertungsbutton
     * @param view
     */
    public void openAuswertungsActivity(View view) {
        startActivity(new Intent(this, AuswertenActivity.class));
    }


    /**
     *  checks dangerous permissions if allowed. If not, it shows a dialog to explain, why the permissions were important.
     */
    private void checkPermissionsOnRuntime() {

        final int permission_WRITE_EXTERNAL_STORAGE = ContextCompat.checkSelfPermission(context,
                Manifest.permission.WRITE_EXTERNAL_STORAGE);

        final int permission_CAMERA;
        permission_CAMERA = ContextCompat.checkSelfPermission(context,
                Manifest.permission.CAMERA);


        if (permission_CAMERA == -1 ||  permission_WRITE_EXTERNAL_STORAGE == -1) {
            AlertDialog.Builder builder = new AlertDialog.Builder(context);

            builder.setMessage("Es werden zur Benutzung der App folgende Berechtigungen benötigt:\n\nKamera: Für visuelle Warnungen mittels Blitzlicht.\n\nExterner Speicher: Um die aufgezeichneten Messdaten im Telefon zu speichern.");
            builder.setCancelable(false);

            builder.setPositiveButton("Berechtigungen abfragen", new DialogInterface.OnClickListener() {
                public void onClick(DialogInterface dialog, int id) {


                    if (permission_WRITE_EXTERNAL_STORAGE == -1) {
                        ActivityCompat.requestPermissions((Activity) context,
                                new String[]{Manifest.permission.WRITE_EXTERNAL_STORAGE},
                                MY_PERMISSIONS_REQUEST_WRITE_EXTERNAL_STORAGE);
                    }

                    if (permission_CAMERA == -1) {
                        ActivityCompat.requestPermissions((Activity) context,
                                new String[]{Manifest.permission.CAMERA},
                                MY_PERMISSIONS_REQUEST_CAMERA);
                    }

                }
            });
            builder.setNegativeButton("App beenden", new DialogInterface.OnClickListener() {
                public void onClick(DialogInterface dialog, int id) {

                    finish();
                }
            });

            builder.show();
        }


    }


}