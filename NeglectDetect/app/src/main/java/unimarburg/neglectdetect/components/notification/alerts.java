package unimarburg.neglectdetect.components.notification;

import android.content.Context;
import android.content.pm.PackageManager;
import android.hardware.Camera;
import android.media.AudioManager;
import android.media.SoundPool;
import android.os.Handler;
import android.os.Vibrator;
import android.util.Log;

import unimarburg.neglectdetect.R;
import unimarburg.neglectdetect.components.general.Values;

/**
 * Created by Martin on 03.11.2016.
 * Stellt verschiedene Benachrichtigungsmoeglichkeiten zur Verfuegung.
 * Beispielsweise das Blinken der Taschenlampe oder einen Vibrationsalarm
 */

public class alerts {
    private Context context;
    private Camera flashCam;
    private boolean isVibrating;
    private Handler handlerFlash = new Handler();
    public SoundPool test = new SoundPool (1, AudioManager.STREAM_MUSIC, 0);

    Runnable flashOn = new Runnable() {
        @Override
        public void run() {
            Log.i("FlashAlert", "Taschenlampe an");
            if (flashCam == null) return;
            Camera.Parameters p = flashCam.getParameters();
            p.setFlashMode(Camera.Parameters.FLASH_MODE_TORCH);
            flashCam.setParameters(p);
            handlerFlash.postDelayed(flashOff, 1000);               //Nach 1 Sekunde ausschalten
        }
    };
    Runnable flashOff = new Runnable() {
        @Override
        public void run() {
            Log.i("FlashAlert", "Taschenlampe aus");
            if (flashCam == null) return;
            Camera.Parameters p = flashCam.getParameters();
            p.setFlashMode(Camera.Parameters.FLASH_MODE_OFF);
            flashCam.setParameters(p);
            handlerFlash.postDelayed(flashOn, 2000);                //Alle 2 Sekunden einschalten
        }
    };


    public alerts(Context context) {this.context = context;}

    /**
     * Benachrichtigt den Nutzer mit einem fest eingestellten Alarm
     */
    public void soundTest () {
        if(Values.isMute())
        {
            return;
        }
        final int ID = test.load(context, R.raw.glocke, 1);
        this.test.setOnLoadCompleteListener(new SoundPool.OnLoadCompleteListener() {
                @Override
                public void onLoadComplete(SoundPool soundPool, int sampleId, int status) {
                    test.play(ID, 1, 1, 0, 0, 1);
                }
        });

    }

    /**
     * Wenn eine Kamera vorhanden ist wird die Benachrichtigung per Taschenlampe ein-/ausgeschaltet
     */
    public void toggleFlashTest () {
        if(!Values.isFlash())
        {
            return;
        }
        // Kamera vorhanden?
        if (!context.getPackageManager().hasSystemFeature(PackageManager.FEATURE_CAMERA)) {
            Log.e("FlashAlert", "Keine Taschenlampe verfuegbar");
            return;
        }

        if (flashCam == null) {
            Log.e("FlashAlert", "Taschenlampe gestartet");
            flashCam = Camera.open();
            flashCam.startPreview();
            flashOn.run();
        } else {
            Log.e("FlashAlert", "Taschenlampe abgeschaltet");
            flashCam.getParameters().setFlashMode(Camera.Parameters.FLASH_MODE_OFF);
            flashCam.setParameters(flashCam.getParameters());
            flashCam.stopPreview();
            flashCam.release();
            flashCam = null;
            handlerFlash.removeCallbacksAndMessages(null);
        }
    }

    /**
     * Wenn Vibrationsalarm verfuegbar, wird der Nutzer per Vibrationsalarm informiert
     */
    public void vibrationTest () {
        if(!Values.isVibrate())
        {
            return;
        }
        Vibrator v = (Vibrator) this.context.getSystemService(Context.VIBRATOR_SERVICE);

        if (!v.hasVibrator()) {
            Log.e("VibrationAlert", "Kein Vibrationsalarm verfügbar.");
            return;
        }

        if (!isVibrating) {
            long[] pattern = {0, 100, 1000};  //ohne Verzögerung, 100 Millisec. Vibrieren, 1000 Millisec. schlafen
            v.vibrate(pattern, 0);
            isVibrating = true;
        } else {
            v.cancel();
            isVibrating = false;
        }
    }

    public boolean isFlashTestRunning() {return (flashCam != null);}

    public boolean isVibrating() {
        return isVibrating;
    }

    public void setVibrating(boolean vibrating) {
        isVibrating = vibrating;
    }
}
