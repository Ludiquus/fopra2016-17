package unimarburg.neglectdetect.components.input;

import android.os.Build;
import android.os.Environment;

import java.io.File;

/**
 * Created by Martin on 13.03.2017.
 * Stellt verschiedene Pfade und Arbeitsordner zum vereinfachten Zugriff zur Verfuegung
 */

public class FilePath {

    /**
     * Vereinfacht den Zugriff auf den Protokollordner
     * @return
     */
    public static File getWorkingDirectory() {
        /*
        Zum Abfangen des Fehlers: FATAL EXCEPTION: main
        java.lang.NoSuchFieldError: android.os.Environment.DIRECTORY_DOCUMENTS
        at SensorActivity.onBackPressed(SensorActivity.java:198)
        Prüfen, ob API >= 19
        */
        if (Build.VERSION.SDK_INT >= 19) {
            return new File(Environment.getExternalStoragePublicDirectory(Environment.DIRECTORY_DOCUMENTS), "NeglectDetect");
        } else {
            return new File(Environment.getExternalStorageDirectory() + "/Documents", "NeglectDetect");
        }
    }

    /**
     * Vereinfacht den Zugriff auf den Protokollordner(SignificantData).
     * @return
     */
    public static File getWorkingDirectory_SignificantData() {
        if (Build.VERSION.SDK_INT >= 19) {
            return new File(Environment.getExternalStoragePublicDirectory(Environment.DIRECTORY_DOCUMENTS), "NeglectDetect");
        } else {
            return new File(Environment.getExternalStorageDirectory() + "/Documents", "NeglectDetect/SignificantData");
        }
    }
}
