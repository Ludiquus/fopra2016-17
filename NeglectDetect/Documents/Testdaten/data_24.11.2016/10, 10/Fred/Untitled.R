data1 <- read.csv("Test (3).csv",sep="\t",header = T)

plot.new()
plot.window(xlim = c(150,470), ylim = c(-10,10))
lines(data1$Index, data1$AccX,type = "l")
axis(2)
title(ylab="Gemessene Beschleunigung")
lines(data2$Index, data2$AccX,type = "l", col="red")
legend("topleft", c("Schräggehen","Geradegehen"), col = c("black","red"), pch = c("_"))

data2 <- read.csv("Test (3).csv", sep = "\t", header = T)
plot(data1$Index, data1$AverageAccX,type = "l")
lines(data2$Index, data2$AverageAccX,type = "l", col = "red")
