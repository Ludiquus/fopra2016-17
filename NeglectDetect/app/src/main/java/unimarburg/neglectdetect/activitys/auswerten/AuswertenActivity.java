package unimarburg.neglectdetect.activitys.auswerten;

import android.content.DialogInterface;
import android.content.Intent;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v7.app.AlertDialog;
import android.support.v7.app.AppCompatActivity;
import android.util.Log;
import android.view.MenuItem;
import android.view.View;
import android.widget.AdapterView;
import android.widget.ListView;
import android.widget.SimpleAdapter;
import android.widget.Toast;

import java.io.File;
import java.io.InputStream;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;

import unimarburg.neglectdetect.R;
import unimarburg.neglectdetect.components.input.FilePath;

/**
 * Created by Martin on 13.03.2017.
 * Zeigt verfuegbare/auswertbare Protokolle in einer Liste an.
 */

public class AuswertenActivity extends AppCompatActivity {

    public InputStream inputStream;

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_auswerten);
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);

        final List<HashMap<String, String>> listData = generateListData();

        ListView listView = (ListView) findViewById(R.id.listView);
        SimpleAdapter listAdapter = new SimpleAdapter(this, listData, R.layout.listview_auswerten,
                new String[]{"txt"}, new int[]{R.id.text});
        listView.setAdapter(listAdapter);
        listAdapter.notifyDataSetChanged();
        //Zeigt eine Liste aller Logdaten der App, wleche auf dem Gerät vorhanden sind.
        if (listData.size() == 0) {
            new AlertDialog.Builder(this)
                    .setTitle("Kein Protokoll gefunden")
                    .setMessage("Führen sie vorerst einige Messungen durch und versuchen sie es dann nochmal.")
                    .setCancelable(false)
                    .setPositiveButton("Zurück", new DialogInterface.OnClickListener() {
                        @Override
                        public void onClick(DialogInterface dialogInterface, int i) {
                            onBackPressed();
                        }
                    })
                    .show();
        }
        //Wählt ein Listenitem aus, um die beiliegende Datei zur Auswertung zu öffnen
        listView.setOnItemClickListener(new AdapterView.OnItemClickListener() {

            @Override
            public void onItemClick(final AdapterView<?> parent, final View view, final int position, final long id) {
                String pfad = listData.get(position).get("path");
                try {

                    Intent intent = new Intent(AuswertenActivity.this, LoadingFileActivity.class);
                    intent.putExtra("path", pfad);
                    Log.d("FileManager","Opening file: "+pfad);
                    startActivity(intent);
                    //onBackPressed(); vorerst soll nach dem Schließen der alten Datei wieder zur Dateiübersicht zurückgegangen werden.
                } catch (Exception e) {
                    Toast.makeText(AuswertenActivity.this, "Ein Fehler ist aufgetreten.", Toast.LENGTH_LONG).show();
                    e.printStackTrace();
                }
            }
        });
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()) {
            case android.R.id.home:
                onBackPressed();
                break;
        }
        return super.onOptionsItemSelected(item);
    }

    /**
     * Generiert Listendaten. Dafuer wird der Protokollordner geoeffnet und alle Dateien(keine Ordner) gesammelt und fuer die Liste zusammengepackt
     * In der Hashmap befindet sich neben dem Dateinamen auch der vollstaendige Pfad zur Datei
     * @return
     */
    private List<HashMap<String, String>> generateListData() {
        List<HashMap<String, String>> result = new ArrayList<>();

        for (File f : FilePath.getWorkingDirectory().listFiles()) {
            if (f.isDirectory() || !f.getName().contains("_") || f.getName().startsWith("sig_")) continue;
            HashMap<String, String> hm = new HashMap<>();
            String[] nameSplit = f.getName().split("_");
            if (nameSplit.length <= 2) continue;
            //hm.put("txt", nameSplit[2]); TODO hat in der anzeige rumgesponnen daher vorerst auskommentiert
            hm.put("txt",f.getName());
            hm.put("path", f.getAbsolutePath());
            result.add(hm);
        }

        return result;
    }
}
