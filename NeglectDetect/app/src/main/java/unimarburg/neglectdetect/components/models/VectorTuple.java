package unimarburg.neglectdetect.components.models;

/**
 * Created by Johannes on 03.11.2016.
 */

public class VectorTuple {
    private Vector acc;
    private String timestamp;
    private double averageAcc;
    private boolean flagged;

    public boolean isFlagged() {
        return flagged;
    }

    public void setFlagged(boolean flagged) {
        this.flagged = flagged;
    }

    public VectorTuple(Vector a, String time, double avAcc, boolean f)
    {
        this.acc = a;
        this.timestamp = time;
        this.averageAcc=avAcc;
        this.flagged = f;
    }

    public VectorTuple(VectorTuple copy)
    {
        this.acc = copy.getAcc();
        this.timestamp = copy.getTimestamp();
        this.averageAcc = copy.getAverageAcc();
        this.flagged = copy.isFlagged();
    }

    public Vector getAcc() {
        return acc;
    }

    public void setAcc(Vector acc) {
        this.acc = acc;
    }

    public String getTimestamp() {
        return timestamp;
    }

    public void setTimestamp(String timestamp) {
        this.timestamp = timestamp;
    }

    public double getAverageAcc() {
        return averageAcc;
    }

    public void setAverageAcc(double averageAcc) {
        this.averageAcc = averageAcc;
    }
}
