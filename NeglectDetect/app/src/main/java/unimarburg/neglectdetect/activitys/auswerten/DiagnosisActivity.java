package unimarburg.neglectdetect.activitys.auswerten;

import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.widget.TextView;
import android.widget.Toast;

import unimarburg.neglectdetect.R;

public class DiagnosisActivity extends AppCompatActivity {

    TextView countView, lagView, frequencyView, durationView, curveView, avgAccXDiffView;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_diagnosis);

        countView = (TextView) findViewById(R.id.diagnosis_countView);
        frequencyView = (TextView) findViewById(R.id.diagnosis_frequencyView);
        durationView = (TextView) findViewById(R.id.diagnosis_DurationView);
        curveView = (TextView) findViewById(R.id.diagnosis_curveView);
        avgAccXDiffView = (TextView) findViewById(R.id.diagnosis_avgAccXDiffView);

        Bundle bundle = getIntent().getExtras();
        int count = bundle.getInt("count");
        if(count == 0)
        {
            Toast.makeText(this,"Keine Fehlverhalten festgestellt!", Toast.LENGTH_LONG).show();
            finish();
        }
        long startTime = bundle.getLong("startTime");
        long endTime = bundle.getLong("endTime");
        countView.setText(""+count);
        double frequency = Math.round((count / ((endTime-startTime)/60000.0))*1000)/1000.0;
        frequencyView.setText(""+frequency);
        durationView.setText(""+ (endTime-startTime)/1000.0+" Sekunden");
        long c = bundle.getLong("gyroSum");
        if(c>=0)
            curveView.setText(""+c);
        else
            curveView.setText("nicht Aufgezeichnet");
        long dataAmount = bundle.getLong("dataAmount");
        avgAccXDiffView.setText(""+(bundle.getDouble("avgAccXDiffSum")/dataAmount));
    }
}
