package unimarburg.neglectdetect.components.output;

import java.io.BufferedWriter;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.OutputStreamWriter;
import java.util.ArrayList;

import unimarburg.neglectdetect.components.models.VectorTuple;

/**
 * Created by Johannes on 03.11.2016.
 */

public class printer {

    /**
     * @author Johannes Ludwig
     * @param list ArrayList mit allen zu speichernden VectorTuple Objekten.
     * @param fos OutputStream zur gewünschten Spicherstelle der .csv Datei
     *
     *            Speichert die übergebenen Daten in Form einer .csv-Datei an der Stelle zu welcher fos führt.
     */
    public static void sensorList(ArrayList<VectorTuple> list, FileOutputStream fos)
    {
        try {
            BufferedWriter bw = new BufferedWriter(new OutputStreamWriter(fos));
            bw.write("Index\tTimestamp\tAccX\tAccY\tAccZ\tAverageAccX\tFlagged");
            bw.newLine();
            bw.flush();
            int i = 1;
            for(VectorTuple v: list)
            {

                bw.write(i+"\t"+v.getTimestamp()+"\t"+v.getAcc().getX()+"\t" +v.getAcc().getY()+"\t" +v.getAcc().getZ()+"\t"+v.getAverageAcc()+"\t"+(v.isFlagged() ? 1 : 0));
                bw.newLine();
                bw.flush();
                i++;
            }
            bw.flush();
            bw.close();
        }catch(IOException e)
        {
            e.printStackTrace();
        }
    }
}
