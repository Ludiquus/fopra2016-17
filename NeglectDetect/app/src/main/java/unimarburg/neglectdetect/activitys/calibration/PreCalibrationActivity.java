package unimarburg.neglectdetect.activitys.calibration;
/**
 * Created by Leon on 30.11.2016.
 */
import android.content.Context;
import android.content.Intent;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.TextView;

import unimarburg.neglectdetect.R;
import unimarburg.neglectdetect.components.general.Values;

public class PreCalibrationActivity extends AppCompatActivity {
   public TextView text;
    public String Cal;
    private Context context;

    /**
     * Shows the calibrated, average acceleration for testing purpose.
     * Todo Remove after Testing
     * @author Leon
     */
    public void showCal (){

        Cal = Double.toString(Values.getCalibratedBaseAccX());
        text = (TextView) findViewById(R.id.textView7);
        text.setText("Kalibrierter Wert:" + Cal);
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_precalibration);
        showCal();
    }

    public void startCalibration (View view) {
        Intent intent = new Intent(this, CalibrationActivity.class);
        finish();
        startActivity(intent);
    }

    @Override
    protected void onResume() {

        super.onResume();
        showCal();

    }


}
