package unimarburg.neglectdetect.activitys.messung;

/**
 * Created by Johannes on 03.11.2016.
 */
import android.content.Intent;
import android.hardware.Sensor;
import android.hardware.SensorEvent;
import android.hardware.SensorEventListener;
import android.os.PowerManager;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.widget.CompoundButton;
import android.widget.TextView;
import android.widget.Toast;
import android.widget.ToggleButton;

import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;

import unimarburg.neglectdetect.R;
import unimarburg.neglectdetect.components.input.Sensors;
import unimarburg.neglectdetect.components.general.Values;
import unimarburg.neglectdetect.activitys.auswerten.DiagnosisActivity;
import unimarburg.neglectdetect.components.input.FilePath;
import unimarburg.neglectdetect.components.models.Vector;
import unimarburg.neglectdetect.components.notification.alerts;
import unimarburg.neglectdetect.components.output.printer;
import unimarburg.neglectdetect.components.output.sensorLogger;

public class SensorActivity extends AppCompatActivity {

    public TextView accX, accY, accZ, avAccX;
    SensorEventListener AccListener,GyroListener;
    boolean diagnosis = false;
    int diagnosisErrors = 0;
    long diagnosisStart = 0L;
    long diagnosisLastTrigger = 0L;
    long diagnosisTriggerLagSum = 0L;
    long diagnosisGyroSum = 0L;
    long diagnosisDataAmount = 0L;
    double diagnosisAvgAccXDiffSum = 0.0;

    public alerts alert;
    private sensorLogger sensorLogger;
    PowerManager.WakeLock wakeLock;
    PowerManager powerManager;
    ToggleButton diagnosisButton;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_sensors);
        powerManager = (PowerManager) getSystemService(POWER_SERVICE);
        wakeLock = powerManager.newWakeLock(PowerManager.PARTIAL_WAKE_LOCK, "WakeLock");
        alert = new alerts(this);
        sensorLogger = new sensorLogger(this);
        diagnosisButton = (ToggleButton) findViewById(R.id.toggle_diagnosis);
        diagnosisButton.setOnCheckedChangeListener(new CompoundButton.OnCheckedChangeListener() {
            @Override
            public void onCheckedChanged(CompoundButton buttonView, boolean isChecked) {
                if(isChecked)
                {
                    diagnosis = true;
                    diagnosisStart = System.currentTimeMillis();
                    diagnosisLastTrigger = diagnosisStart;
                }
                else
                {
                    //Diagnose wird vorbereitet
                    diagnosis = false;
                    if(diagnosisErrors>0) {
                        Intent intent = new Intent(SensorActivity.this, DiagnosisActivity.class);
                        intent.putExtra("count", diagnosisErrors);
                        intent.putExtra("startTime", diagnosisStart);
                        intent.putExtra("endTime", System.currentTimeMillis());
                        //intent.putExtra("lagSum", diagnosisTriggerLagSum);
                        intent.putExtra("gyroSum", diagnosisGyroSum);
                        intent.putExtra("avgAccXDiffSum", diagnosisAvgAccXDiffSum);
                        intent.putExtra("dataAmount", diagnosisDataAmount);
                        startActivity(intent);
                        diagnosisErrors = 0;
                        diagnosisStart = 0L;
                        diagnosisLastTrigger = 0L;
                        diagnosisTriggerLagSum = 0L;
                        diagnosisGyroSum = 0L;
                        diagnosisAvgAccXDiffSum = 0.0;
                        diagnosisDataAmount = 0L;
                    }
                    else
                    {
                        Toast.makeText(SensorActivity.this, "Keine Fehlverhalten erkannt", Toast.LENGTH_SHORT).show();
                    }
                }
            }
        });


        AccListener = new SensorEventListener(){

            @Override
            public void onSensorChanged(SensorEvent event) {
                sensorLogger.acc = new Vector(event.values[0], event.values[1],event.values[2]);
                sensorLogger.writeVectorTuple();
            }

            @Override
            public void onAccuracyChanged(Sensor sensor, int i) {

            }
        };

        GyroListener = new SensorEventListener(){

            @Override
            public void onSensorChanged(SensorEvent event) {
                sensorLogger.gyro = new Vector(event.values[0], event.values[1],event.values[2]);
            }

            @Override
            public void onAccuracyChanged(Sensor sensor, int i) {

            }
        };


        accX =(TextView) findViewById(R.id.accelViewX);
        accY =(TextView) findViewById(R.id.accelViewY);
        accZ =(TextView) findViewById(R.id.accelViewZ);
        avAccX = (TextView) findViewById(R.id.avAccXView);
    }

    public void gyroTrigger()
    {
        diagnosisGyroSum++;
    }

    public void addAvgAccXSum(double n)
    {
        diagnosisAvgAccXDiffSum+=n;
        diagnosisDataAmount++;
    }

    public void alert()
    {
        operateDiagnosis();
        if(!diagnosis) {
            if (!alert.isVibrating()) {
                alert.vibrationTest();
            }
            if (!alert.isFlashTestRunning()) {
                alert.toggleFlashTest();
            }

            alert.soundTest();
        }
    }

    //Führt Funktionen des Diagnosemodus aus solange dieser eingeschaltet ist.
    private void operateDiagnosis()
    {
        diagnosisErrors++;
        long t = System.currentTimeMillis();
        diagnosisTriggerLagSum += t-diagnosisLastTrigger;
        diagnosisLastTrigger = t;
    }

    public void stopAlerts()
    {
        if(alert.isVibrating())
        {
            alert.vibrationTest();
        }
        if(alert.isFlashTestRunning())
        {
            alert.toggleFlashTest();
        }
    }



    @Override
    protected void onResume() {
        super.onResume();
        wakeLock.acquire();
        Sensors.getSManager().registerListener(AccListener,Sensors.getAcceleration(),sensorLogger.sensorDelay);
        Sensors.getSManager().registerListener(GyroListener,Sensors.getGyro(),sensorLogger.sensorDelay);
    }

    @Override
    protected void onPause() {
        super.onPause();
        wakeLock.release();
        Sensors.getSManager().unregisterListener(AccListener);
        Sensors.getSManager().unregisterListener(GyroListener);

        stopAlerts();
    }

    @Override
    public void onBackPressed() {
        super.onBackPressed();
        //Wird die Activiy beendet werden alle geloggten Daten in einer .csv Datei gespeichert.
        if(Values.isSaveInCSV()) {
            File path;

            path = FilePath.getWorkingDirectory();

            try {
                File file = new File(path, Values.getUser() + "_" + sensorLogger.tuples.get(0).getTimestamp() + ".csv"); //Im Emulator stürzt App hier ab, wenn kein Fehlverhalten erkannt und vermutlich keine Daten gemessen wurden. Unbekannt, ob auch auf echten Handy
                path.mkdirs();
                file.createNewFile();
                FileOutputStream fos = new FileOutputStream(file);
                printer.sensorList(sensorLogger.tuples, fos);
            } catch (FileNotFoundException e) {
                e.printStackTrace();
            } catch (IOException ioe) {
                ioe.printStackTrace();
            }

            finish();
        }
    }
}

