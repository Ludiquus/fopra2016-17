package unimarburg.neglectdetect.components.general;

import android.content.Context;

import java.io.BufferedReader;
import java.io.BufferedWriter;
import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStreamReader;
import java.io.OutputStreamWriter;

/**
 * Created by vincentcuccu on 28.11.16.
 */

public class Values {

    /**
     * Values
     */

    private static boolean vibrate, mute, flash; //PreferencesActivity

    private static int calcRate; //GPSActivity


    private static String user;

    private static boolean saveInCSV = true; //true -> save in csv

    private static double calibratedBaseAccX;

    private static boolean motionStep = true;   //true = Step Sensor, false= Motion Sensor

    private static boolean handyRight = false; //false = Handy links befestigt, true = rechts




    //@author Johannes
    //from here on all debug Values that will be loaded by the Activities is loaded from the Internal Storage and saved globally.
    //@value tupleSize determines how many VectorTuple Objects are used to eval the average acceleration on the X-Axis
    //@value sensorDelay determines the Delay before every implemented Sensor's Data will be saved into a VectorTuple Object. Unit is microsecs
    //@value avgXcount counts after how many saved Tuples a new average acceleration on the X-Axis is evaled out of the last tupleSize tuples.
    //@value dataTimeOut determines how many Tuples are ignored after the Gyroscope triggers.
    //@value alertScore determines how many alert points must be gathered before the Device triggers any alerts.
    private static int  tupleSize = 250, sensorDelay = 20000,
                        avgAccXcount = 50, dataTimeOut = 10,
                        alertScore = 15;

    private static double gyroTolerance = 0.8, accXTolerance = 0.1;

    public static void saveDebug(Context context)
    {
        try {
            FileOutputStream fos = context.openFileOutput("debug.txt", Context.MODE_PRIVATE);
            BufferedWriter bw = new BufferedWriter(new OutputStreamWriter(fos));
            bw.write(""+tupleSize);
            bw.newLine();
            bw.write(""+sensorDelay);
            bw.newLine();
            bw.write(""+avgAccXcount);
            bw.newLine();
            bw.write(""+dataTimeOut);
            bw.newLine();
            bw.write(""+alertScore);
            bw.newLine();
            bw.write(""+accXTolerance);
            bw.newLine();
            bw.write(""+gyroTolerance);
            bw.newLine();
            bw.flush();
            bw.close();
        }catch(IOException e)
        {
            e.printStackTrace();
        }
    }

    public static void loadDebug(Context context)
    {

        try{
            FileInputStream fis = context.openFileInput("debug.txt");
            BufferedReader br = new BufferedReader(new InputStreamReader(fis));
            String s = "";
            s = br.readLine();
            tupleSize = Integer.parseInt(s);
            s = br.readLine();
            sensorDelay = Integer.parseInt(s);
            s = br.readLine();
            avgAccXcount = Integer.parseInt(s);
            s = br.readLine();
            dataTimeOut = Integer.parseInt(s);
            s = br.readLine();
            alertScore = Integer.parseInt(s);

            s = br.readLine();
            accXTolerance = Double.parseDouble(s);
            s = br.readLine();
            gyroTolerance = Double.parseDouble(s);
            br.close();

        }catch(IOException e)
        {
            loadDefaultDebug(context);
            e.printStackTrace();
        }
    }

    public static void loadDefaultDebug(Context context)
    {
        tupleSize = 250;
        sensorDelay = 20000;
        avgAccXcount = 50;
        dataTimeOut = 50;
        alertScore = 15;
        gyroTolerance = 0.8;
        accXTolerance = 0.1;
        saveDebug(context);
    }



    public static void save(Context context) {
        try {
            FileOutputStream fos = context.openFileOutput("settings.txt", Context.MODE_PRIVATE);
            BufferedWriter bw = new BufferedWriter(new OutputStreamWriter(fos));
            if(vibrate)
            {
                bw.write("1");
            }
            else
            {
                bw.write("0");
            }
            bw.newLine();

            if(mute)
            {
                bw.write("1");
            }
            else
            {
                bw.write("0");
            }
            bw.newLine();

            if(flash)
            {
                bw.write("1");
            }
            else
            {
                bw.write("0");
            }
            bw.newLine();

            if(saveInCSV)
            {
                bw.write("1");
            }
            else
            {
                bw.write("0");
            }
            bw.newLine();

            bw.write(""+calcRate);
            bw.newLine();

            bw.write(""+calibratedBaseAccX);
            bw.newLine();

            bw.write(""+motionStep);
            bw.newLine();

            bw.write(""+handyRight);
            bw.newLine();

            bw.write(""+user);
            bw.newLine();
            bw.flush();
            bw.close();

        }catch(IOException e)
        {
            e.printStackTrace();
        }
    }

    public static void load(Context context) {

        try{
            FileInputStream fis = context.openFileInput("settings.txt");
            BufferedReader br = new BufferedReader(new InputStreamReader(fis));
            String s = "";
            s = br.readLine();
            if(s.equals("1"))
            {
                vibrate = true;
            }
            else
            {
                vibrate = false;
            }

            s = br.readLine();
            if(s.equals("1"))
            {
                mute = true;
            }
            else
            {
                mute = false;
            }

            s = br.readLine();
            if(s.equals("1"))
            {
                flash = true;
            }
            else
            {
                flash = false;
            }

            s = br.readLine();
            if(s.equals("1"))
            {
                saveInCSV = true;
            }
            else
            {
                saveInCSV = false;
            }

            s = br.readLine();
            calcRate = Integer.parseInt(s);

            s = br.readLine();
            calibratedBaseAccX = Double.parseDouble(s);

            s = br.readLine();
            motionStep = Boolean.valueOf(s);

            s = br.readLine();
            handyRight = Boolean.valueOf(s);

            s = br.readLine();
            user = s;

            br.close();

        }catch(IOException e)
        {
            loadDefaults(context);
            e.printStackTrace();
        }
    }

    public static void loadDefaults(Context context)
    {
        vibrate = true;
        mute = true;
        flash = false;
        calcRate = 20;
        calibratedBaseAccX = 0.1;
        save(context);
    }

    /**
     * getter und setter
     */

    //PreferencesActivity

    public static boolean isVibrate() {
        return vibrate;
    }

    public static void setVibrate(boolean vib) {
        vibrate = vib;
    }

    public static boolean isMute() {
        return mute;
    }

    public static void setMute(boolean m) {
        mute = m;
    }

    public static boolean isHandyRight() {
        return handyRight;
    }

    public static void setHandyRight(boolean handyLeft) {
        Values.handyRight = handyLeft;
    }

    public static boolean isFlash() {
        return flash;
    }

    public static void setFlash(boolean f) {
        flash = f;
    }

    public static boolean isStep() {
        return motionStep;
    }

    public static void setMotionStep(boolean motionStep) {
        Values.motionStep = motionStep;
    }

    public static String getUser() {
        return user;
    }

    public static void setUser(String user) {
        Values.user = user;
    }
    //GPSActivity


    //CalibrationActivity

    //CSV
    public static boolean isSaveInCSV() {
        return saveInCSV;
    }

    public static void setSaveInCSV(boolean saveInCSV) {
        Values.saveInCSV = saveInCSV;
    }

    public static double getCalibratedBaseAccX() {
        return calibratedBaseAccX;
    }

    public static void setCalibratedBaseAccX(double calibratedBaseAccX) {
        Values.calibratedBaseAccX = calibratedBaseAccX;
    }


    //Debug
    public static int getSensorDelay() {
        return sensorDelay;
    }

    public static void setSensorDelay(int sensorDelay) {
        Values.sensorDelay = sensorDelay;
    }

    public static int getAlertScore() {
        return alertScore;
    }

    public static void setAlertScore(int alertScore) {
        Values.alertScore = alertScore;
    }

    public static int getDataTimeOut() {
        return dataTimeOut;
    }

    public static void setDataTimeOut(int dataTimeOut) {
        Values.dataTimeOut = dataTimeOut;
    }

    public static int getAvgAccXcount() {
        return avgAccXcount;
    }

    public static void setAvgAccXcount(int avgAccXcount) {
        Values.avgAccXcount = avgAccXcount;
    }

    public static int getTupleSize() {
        return tupleSize;
    }

    public static void setTupleSize(int tupleSize) {
        Values.tupleSize = tupleSize;
    }

    public static double getGyroTolerance() {
        return gyroTolerance;
    }

    public static void setGyroTolerance(double gyroTolerance) {
        Values.gyroTolerance = gyroTolerance;
    }

    public static double getAccXTolerance() {
        return accXTolerance;
    }

    public static void setAccXTolerance(double accXTolerance) {
        Values.accXTolerance = accXTolerance;
    }
}
