\select@language {ngerman}
\contentsline {section}{\numberline {1}Aufgabenstellung und Problemanalyse}{4}{section.1}
\contentsline {section}{\numberline {2}Problemanalyse}{6}{section.8}
\contentsline {subsection}{\numberline {2.1}L\IeC {\"o}sungsans\IeC {\"a}tze}{6}{subsection.9}
\contentsline {subsubsection}{\numberline {2.1.1}Global Positioning System (GPS)}{6}{subsubsection.10}
\contentsline {subsubsection}{\numberline {2.1.2}Accelerometer}{7}{subsubsection.11}
\contentsline {subsubsection}{\numberline {2.1.3}Kompass und Gyroskop}{7}{subsubsection.12}
\contentsline {subsection}{\numberline {2.2}Verworfene Ans\IeC {\"a}tze}{8}{subsection.13}
\contentsline {section}{\numberline {3}Technischer Entwurf}{9}{section.14}
\contentsline {subsection}{\numberline {3.1}Gesamt\IeC {\"u}bersicht}{9}{subsection.15}
\contentsline {subsubsection}{\numberline {3.1.1}Statische Systemstruktur}{9}{subsubsection.16}
\contentsline {subsubsubsection}{\numberline {3.1.1.1}Activitys}{9}{subsubsubsection.17}
\contentsline {subsubsubsection}{\numberline {3.1.1.2}Komponenten}{10}{subsubsubsection.19}
\contentsline {subsubsubsection}{\numberline {3.1.1.3}Paketdiagramm}{10}{subsubsubsection.20}
\contentsline {subsubsection}{\numberline {3.1.2}dynamische Systemstruktur}{11}{subsubsection.22}
\contentsline {subsubsubsection}{\numberline {3.1.2.1}Beziehungen zwischen den Activitys}{11}{subsubsubsection.23}
\contentsline {subsubsubsection}{\numberline {3.1.2.2}Beziehungen zwischen den Komponenten}{11}{subsubsubsection.24}
\contentsline {subsubsubsection}{\numberline {3.1.2.3}Beziehungen zwischen den Activitys und den Komponenten}{11}{subsubsubsection.25}
\contentsline {subsubsubsection}{\numberline {3.1.2.4}Aktivit\IeC {\"a}tsdiagramm}{12}{subsubsubsection.26}
\contentsline {subsection}{\numberline {3.2}\IeC {\"U}bergreifende Konzepte}{12}{subsection.28}
\contentsline {subsubsection}{\numberline {3.2.1}Entwurfsentscheidungen}{12}{subsubsection.29}
\contentsline {subsubsection}{\numberline {3.2.2}Durchg\IeC {\"a}ngig verwendete Standards}{13}{subsubsection.30}
\contentsline {subsection}{\numberline {3.3}Spezifikation der Systembausteine}{14}{subsection.31}
\contentsline {subsubsection}{\numberline {3.3.1}Activitys}{14}{subsubsection.32}
\contentsline {subsubsection}{\numberline {3.3.2}Components}{15}{subsubsection.33}
\contentsline {section}{\numberline {4}Realisierung}{18}{section.34}
\contentsline {subsection}{\numberline {4.1}Testen der Activitys}{18}{subsection.35}
\contentsline {subsubsection}{\numberline {4.1.1}\texttt {DebugActivity}}{18}{subsubsection.36}
\contentsline {subsubsection}{\numberline {4.1.2}\texttt {MenuActivity} / \texttt {SensorActivity} / \texttt {DiagnosisActivity}}{21}{subsubsection.46}
\contentsline {subsubsection}{\numberline {4.1.3}\texttt {LoadingFileActivity}}{27}{subsubsection.59}
\contentsline {subsubsection}{\numberline {4.1.4}\texttt {CalibrationActivity} / \texttt {PreCalibrationActivity}}{31}{subsubsection.68}
\contentsline {subsection}{\numberline {4.2}Testen der Erkennung von Schr\IeC {\"a}gg\IeC {\"a}ngen }{32}{subsection.73}
\contentsline {section}{\numberline {5}Installation und Einsatz}{37}{section.74}
\contentsline {subsection}{\numberline {5.1}Systemanforderungen}{37}{subsection.75}
\contentsline {subsection}{\numberline {5.2}Installation und Initialisierung der mobile Anwendung }{37}{subsection.76}
\contentsline {subsection}{\numberline {5.3}Positionierung des Ger\IeC {\"a}ts w\IeC {\"a}hrend der Benutzung}{38}{subsection.86}
\contentsline {subsection}{\numberline {5.4}Verwendung der mobile Anwendung}{38}{subsection.87}
\contentsline {subsubsection}{\numberline {5.4.1}Erste Inbetriebnahme}{38}{subsubsection.88}
\contentsline {subsubsection}{\numberline {5.4.2}Einstellungen}{39}{subsubsection.92}
\contentsline {subsection}{\numberline {5.5}Messung durchf\IeC {\"u}hren}{40}{subsection.93}
\contentsline {subsubsection}{\numberline {5.5.1}Standardmessung}{40}{subsubsection.94}
\contentsline {subsubsection}{\numberline {5.5.2}Diagnosemodus}{41}{subsubsection.95}
\contentsline {subsection}{\numberline {5.6}Auswertung von Messdaten}{41}{subsection.96}
\contentsline {section}{Literatur}{42}{figure.caption.97}
\contentsline {section}{Abbildungsverzeichnis}{42}{figure.caption.97}
