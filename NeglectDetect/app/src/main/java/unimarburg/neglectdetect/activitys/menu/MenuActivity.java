package unimarburg.neglectdetect.activitys.menu;

import android.app.Activity;
import android.content.Intent;
import android.os.Bundle;
import android.text.Editable;
import android.text.TextWatcher;
import android.view.View;
import android.widget.CheckBox;
import android.widget.CompoundButton;
import android.widget.EditText;
import android.widget.Switch;
import android.widget.TextView;

import unimarburg.neglectdetect.R;
import unimarburg.neglectdetect.components.general.Values;
import unimarburg.neglectdetect.activitys.calibration.PreCalibrationActivity;

/**
 * Created by Johannes on 09.12.2016.
 */

public class MenuActivity extends Activity {
    CheckBox sound,log,flash,vibration;
    TextView cba;
    Switch switchMotionStep, switchPosition;
    EditText userEdit;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        setContentView(R.layout.activity_menu);

        cba = (TextView) findViewById(R.id.CBAView);
        sound = (CheckBox) findViewById(R.id.checkSound);
        log = (CheckBox) findViewById(R.id.checkLog);
        vibration = (CheckBox) findViewById(R.id.checkVib);
        flash = (CheckBox) findViewById(R.id.checkFlash);

        userEdit = (EditText) findViewById(R.id.editName);

        flash.setChecked(Values.isFlash());
        sound.setChecked(!Values.isMute());
        vibration.setChecked(Values.isVibrate());
        log.setChecked(Values.isSaveInCSV());
        cba.setText(""+Values.getCalibratedBaseAccX());

        userEdit.setText(Values.getUser());

        switchMotionStep = (Switch) findViewById(R.id.switch1);
        switchMotionStep.setChecked(Values.isStep());

        switchPosition = (Switch) findViewById(R.id.switch2);
        switchPosition.setChecked(Values.isHandyRight());

        log.setOnCheckedChangeListener(new CompoundButton.OnCheckedChangeListener() {
            @Override
            public void onCheckedChanged(CompoundButton compoundButton, boolean b) {
                Values.setSaveInCSV(log.isChecked());
            }
        });

        vibration.setOnCheckedChangeListener(new CompoundButton.OnCheckedChangeListener() {
            @Override
            public void onCheckedChanged(CompoundButton compoundButton, boolean b) {
                Values.setVibrate(vibration.isChecked());
            }
        });

        sound.setOnCheckedChangeListener(new CompoundButton.OnCheckedChangeListener() {
            @Override
            public void onCheckedChanged(CompoundButton compoundButton, boolean b) {
                Values.setMute(!sound.isChecked());
            }
        });

        flash.setOnCheckedChangeListener(new CompoundButton.OnCheckedChangeListener() {
            @Override
            public void onCheckedChanged(CompoundButton compoundButton, boolean b) {
                Values.setFlash(flash.isChecked());
            }
        });

        switchMotionStep.setOnCheckedChangeListener(new CompoundButton.OnCheckedChangeListener() {
            @Override
            public void onCheckedChanged(CompoundButton compoundButton, boolean b) {
                Values.setMotionStep(b);
            }
        });

        switchPosition.setOnCheckedChangeListener(new CompoundButton.OnCheckedChangeListener(){
            @Override
            public void onCheckedChanged(CompoundButton compoundButton, boolean b){
                Values.setHandyRight(b);
            }
        }
        );

        userEdit.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence s, int start, int count, int after) {

            }

            @Override
            public void onTextChanged(CharSequence s, int start, int before, int count) {
                Values.setUser(s.toString());
            }

            @Override
            public void afterTextChanged(Editable s) {

            }
        });


    }

    public void onCalPressed(View view)
    {
        Intent intent = new Intent(this, PreCalibrationActivity.class);
        finish();
        startActivity(intent);
    }

    public void gotoDebug(View view)
    {
        Intent intent = new Intent(this, DebugActivity.class);
        startActivity(intent);
    }

    @Override
    public void onBackPressed() {
        Values.save(this);
        finish();
    }
}
