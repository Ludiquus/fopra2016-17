package unimarburg.neglectdetect.components.output;

import android.util.Log;

import java.io.File;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.OutputStreamWriter;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Locale;

import unimarburg.neglectdetect.components.general.Values;
import unimarburg.neglectdetect.activitys.messung.SensorActivity;
import unimarburg.neglectdetect.components.input.FilePath;
import unimarburg.neglectdetect.components.models.SignificantDataTuple;
import unimarburg.neglectdetect.components.models.Vector;
import unimarburg.neglectdetect.components.models.VectorTuple;

/**
 * Created by Martin on 22.11.2016.
 * Loggt signifikante Tupel/Werte
 */

public class sensorLogger {
    private SensorActivity sensorActivity;
    public Vector acc, gyro;
    public ArrayList<Float> avgAngles = new ArrayList<>();
    public int tupleSize, sensorDelay, avgAccXcount, dataTimeOut, alertScore;
    public double gyroTolerance = 0.8, accXTolerance = 0.1;
    public int dataCount = 0, scoreCount = 0, timeOutCount = 0, numberOfExtraValues = 5;
    public double averageAcc = 0, signum;
    private boolean newCurve = true;
    private OutputStreamWriter osw;
    public ArrayList<VectorTuple> tuples = new ArrayList<VectorTuple>(), extraValues = new ArrayList<>();

    public sensorLogger(SensorActivity sensorActivity) {

        if(Values.isHandyRight() == false) signum = -1;
        else signum = 1;

        this.sensorActivity = sensorActivity;
        this.tupleSize = Values.getTupleSize();
        this.sensorDelay = Values.getSensorDelay();
        this.avgAccXcount = Values.getAvgAccXcount();
        this.dataTimeOut = Values.getDataTimeOut();
        this.alertScore = Values.getAlertScore();

        SimpleDateFormat dateFormat = new SimpleDateFormat("dd.MM.yyyy_hh.mm.ss", Locale.GERMANY);
        String timestamp = dateFormat.format(new java.util.Date());
        File path = FilePath.getWorkingDirectory_SignificantData();

        try {
            /**
             * @author Johannes Ludwig
             * Richtet .csv Struktur für SigData ein
             */
            File file = new File(path, "sig_"+Values.getUser()+"_" + timestamp + ".csv");
            path.mkdirs();
            file.createNewFile();
            FileOutputStream fos = new FileOutputStream(file);
            osw = new OutputStreamWriter(fos);

            osw.write("Timestamp");
            osw.write("\t");

            osw.write("alertNumber");
            osw.write("\t");

            osw.write("Vorzeichen");
            osw.write("\t");

            osw.write("Abweichung");
            osw.write("\t");

            osw.write("Trigger?");
            osw.write("\t");

            osw.write("Gyroflag");
            osw.write("\n");

            osw.flush();

            Log.d("Sig_Output:","Initialized succesfully");

        }
        catch (IOException e) {
        }
    }

    /**
     * @author Johannes Ludwig
     * Speichert die aktuellen Daten in einem Objekt des Typs VectorTuple
     */
    public void writeVectorTuple()
    {
        if(acc != null)
        {
            int l = 8;
            String  ax=""+acc.getX(),
                    ay=""+acc.getY(),
                    az=""+acc.getZ();

            if(ax.length()>l) ax = ax.substring(0,l);
            if(ay.length()>l) ay = ay.substring(0,l);
            if(az.length()>l) az = az.substring(0,l);

            sensorActivity.accX.setText(ax);
            sensorActivity.accY.setText(ay);
            sensorActivity.accZ.setText(az);

            SimpleDateFormat dateFormat = new SimpleDateFormat("dd.MM.yyyy_hh.mm.ss", Locale.GERMANY);
            String timestamp = dateFormat.format(new java.util.Date());


            boolean flag = false;
            if(gyro != null)
            {
                // Kurvenerkennung
                //newCurve zeigt ob es sich um den Teil einer bereits erkannten Kurve handelt
                flag = Math.abs(gyro.getY()) >= gyroTolerance;
                if(flag)
                {
                    if(newCurve)
                    {
                        sensorActivity.gyroTrigger();
                    }
                    timeOutCount = dataTimeOut;
                    newCurve = false;
                }
            }
                VectorTuple v = new VectorTuple(acc, timestamp, averageAcc, flag);
                if (timeOutCount == 0) {
                    newCurve = true;
                    dataCount++;

                    if (dataCount >= avgAccXcount) {
                        evalAverageAcc();
                        dataCount = 0;
                        if (tuples.size() >= tupleSize) {
                            //Verarbeitung der Bewegungsdaten
                            double diff = Math.abs(averageAcc - Values.getCalibratedBaseAccX());
                            sensorActivity.addAvgAccXSum(diff);
                            Log.d("Diff:", "" + diff + " Score:" + scoreCount);
                            if (diff >= accXTolerance && Math.signum(diff)==signum) {

                                scoreCount += 5;
                                extractData(v);
                            } else {
                                scoreCount--;
                            }
                            if (scoreCount >= alertScore) {
                                scoreCount = 0;
                                Log.d("Diff:", "Alert startet");
                                sensorActivity.alert();
                            } else {
                                if(extraValues.size() > numberOfExtraValues) {
                                    extraValues.remove(0);
                                }
                                extraValues.add(v);

                                sensorActivity.stopAlerts();
                            }
                            if (scoreCount < 0) {
                                scoreCount = 0;
                            }
                        }
                    }
                    String avAx = "" + averageAcc;
                    if (avAx.length() > l) avAx = avAx.substring(0, l);

                    sensorActivity.avAccX.setText(avAx);
                    //Speichert Tupel in Logdatei wenn in den Optionen festgelegt.
                    //Werden die Daten nicht geloggt werden zu alte Daten gelöscht
                    if (!Values.isSaveInCSV()) {
                        if (tuples.size() > tupleSize) {
                            tuples.remove(0);
                        }
                    }
                    tuples.add(v);

                } else {
                    extractData(v);
                    timeOutCount--;
                }
        }
    }

    /**
     * @author Johannes Ludwig
     * Berechnet durchschnittliche Beschleunigung auf der X-Achse im Zeitraum von tupleSize gesammelten Datensätzen.
     */
    private void evalAverageAcc()
    {
        if(tuples.size()>=tupleSize)
        {
            double accumulation = 0;
            for(int i = tuples.size()-tupleSize;i<tuples.size();i++)
            {
                accumulation += tuples.get(i).getAcc().getX();
            }
            averageAcc = accumulation/tupleSize;
        }
    }

    /**
     * @author Johannes Ludwig
     * Schreibt ein SigDataTupel aus den Daten eines Gegebenen VectorTupels
     */
    private void extractData(VectorTuple vt)
    {
        byte temp =0;
        if(Values.isFlash())
            temp+=1;
        if(!Values.isMute())
            temp+=2;
        if(Values.isVibrate())
            temp+=4;
        SignificantDataTuple sdt = new SignificantDataTuple(vt,temp,true);
        try {
            /* @author Johannes
            Wegen Überflüßigkeit ist die Ausgabe entfernt bis ein Nutzen gefnden werden sollte

            for(VectorTuple v : extraValues) {
                SignificantDataTuple extra = new SignificantDataTuple(v,temp,false);
                extra.write(osw); //schreibt die extra Werte vor dem Trigger-Wert
            }
            */
            sdt.write(osw);

            extraValues = new ArrayList<>();
        } catch (IOException e) {
            e.printStackTrace();
        }

    }

}
